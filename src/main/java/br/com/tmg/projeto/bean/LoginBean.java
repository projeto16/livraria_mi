
package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.ClienteDAO;
import br.com.tmg.projeto.model.Cliente;
import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.inject.Named;

@Named(value = "loginBean")
@ViewScoped
public class LoginBean implements Serializable{
    
    private Cliente cliente = new Cliente();
    ClienteDAO dao = new ClienteDAO();

    public LoginBean() {
    }
    
    
    public String enviar() {
             
            cliente = dao.getCliente(cliente.getEmail(),cliente.getSenha());
            if (cliente == null) {
                  cliente = new Cliente();
                  FacesContext.getCurrentInstance().addMessage(
                             null,
                             new FacesMessage(FacesMessage.SEVERITY_ERROR, "Usuário não encontrado!",
                                         "Erro no Login!"));
                  return null;
            } else {
                  return "/main";
            }
             
             
      }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
    
    
    
    
}

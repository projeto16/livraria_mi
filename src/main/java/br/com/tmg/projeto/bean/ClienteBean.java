
package br.com.tmg.projeto.bean;

import br.com.tmg.projeto.DAO.ClienteDAO;
import br.com.tmg.projeto.model.Cliente;
import java.io.Serializable;
import javax.faces.bean.ViewScoped;
import javax.inject.Named;

@Named(value = "clienteBean")
@ViewScoped
public class ClienteBean implements Serializable{
    
    private Cliente cliente;
    private ClienteDAO dao;

    public ClienteBean() {
        this.cliente = new Cliente();
        this.dao = new ClienteDAO();
    }
    
     public void salvar(){
        if(this.cliente.getId()==0){
            dao.save(cliente);
        }else{
            dao.update(cliente);
        }
               
        
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }
   
     
    
}

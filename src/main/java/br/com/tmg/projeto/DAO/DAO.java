
package br.com.tmg.projeto.DAO;

import java.util.List;
import javax.persistence.EntityManager;


public abstract class DAO<T> {
    
    protected EntityManager em ;
    
    private final Class<T> entidade ;
    
    public  DAO(Class<T> entidade){
        this.entidade = entidade ;
    }
    
    public  void save(T entity){
        this.em = JPAUtil.getEntityManager(); //abrir conexao
        em.getTransaction().begin(); //começar conexao
        em.persist(entity); // inserir
        em.getTransaction().commit(); //finalizar conexão
        em.close(); // fim conexão
        
    }
    
    public void update(T entity){
        this.em = JPAUtil.getEntityManager(); //abrir conexao
        em.getTransaction().begin(); //começar conexao
        em.merge(entity); // misturar
        em.getTransaction().commit(); //finalizar conexão
        em.close(); // fim conexão
        
    }

    public void delete(T entity){
        this.em = JPAUtil.getEntityManager(); //abrir conexao
        em.getTransaction().begin(); //começar conexao
        em.remove(em.merge(entity)); // remover
        em.getTransaction().commit(); //finalizar conexão
        em.close(); // fim conexão
    }

    public T find (long id){
        this.em = JPAUtil.getEntityManager();
        em.getTransaction().begin();
        T t = em.find(entidade, id);
        em.getTransaction().commit();
        em.close();
       
        return t;
    }

    public List<T> findAll (){
        this.em = JPAUtil.getEntityManager();
        List<T> lista;
        em.getTransaction().begin();
        lista= em.createQuery("from " + entidade.getName() + " e").getResultList();
        em.getTransaction().commit();
        em.close();
        
        
        return lista ;
    }
    
    
    
    
    
    
    
    
}

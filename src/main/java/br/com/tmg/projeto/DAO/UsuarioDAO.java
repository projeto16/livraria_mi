
package br.com.tmg.projeto.DAO;

import br.com.tmg.projeto.model.Cliente;
import br.com.tmg.projeto.model.Usuario;
import java.util.List;
import javax.persistence.Query;


public class UsuarioDAO extends DAO<Usuario>{
    
    
    public UsuarioDAO(){
        super(Usuario.class);
    }
    
    public List<Cliente> findByFiltro(String id, String nome) {
        this.em = JPAUtil.getEntityManager();
        List<Cliente> lista;
        em.getTransaction().begin();

        StringBuilder sql = new StringBuilder("from Usuario a where 1=1 ");

        if (id != null && !id.isEmpty()) {
            sql.append(" and a.id = :Id ");
        }

        if (nome != null && !nome.isEmpty()) {
            sql.append(" and a.nome like :Nome ");
        }
        
         Query query = em.createQuery(sql.toString());
        
        if (id!= null && !id.isEmpty()) {
           query.setParameter("Id", new Long(id));
        }

        if (nome != null && !nome.isEmpty()) {
            query.setParameter("Nome", "%" + nome + "%");
        }


        lista = query.getResultList();

        em.getTransaction().commit();
        em.close();

        return lista;
    }
    public static void main(String[] args) {
       
        UsuarioDAO dao = new UsuarioDAO();
        Usuario u = new Usuario();
        u.setCelular("4454545454");
        u.setNome("jgjgggg");
        u.setCidade("hghg");
        u.setCpf("54545454");
        u.setEstado("es");
        u.setNascimento("123648");
        u.setEmail("nbjvvvvh");
        u.setNumero(54);
        
        dao.save(u);
       

    }
}


package br.com.tmg.projeto.model;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Cliente")
public class Cliente extends Pessoa{
   
    
    @OneToMany(targetEntity = Compra.class)
    private List<Compra> compras;

    public Cliente() {
    }

    
    public List<Compra> getCompras() {
        return compras;
    }

    public void setCompras(List<Compra> compras) {
        this.compras = compras;
    }
    
    
    
}

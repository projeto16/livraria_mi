
package br.com.tmg.projeto.model;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity
public class Categoria extends Entidade{
    
    @Column(nullable = false)
    private String Nome;
    
    @Column(nullable = false)
    private String descricao;
    
    @Column(nullable = false)
    private String imagem ;

    public Categoria() {
    }
       

    public String getNome() {
        return Nome;
    }

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }
    
    
}
